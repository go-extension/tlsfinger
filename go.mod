module gitlab.com/go-extension/tlsfinger

go 1.24.0

require gitlab.com/go-extension/tls v0.0.0-20250213181811-2b06719ee5f1

require (
	github.com/RyuaNerin/go-krypto v1.3.0 // indirect
	github.com/andybalholm/brotli v1.1.1 // indirect
	github.com/blang/semver/v4 v4.0.0 // indirect
	github.com/klauspost/compress v1.17.11 // indirect
	github.com/pedroalbanese/camellia v0.0.0-20220911183557-30cc05c20118 // indirect
	github.com/pmorjan/kmod v1.1.1 // indirect
	gitlab.com/go-extension/aes-ccm v0.0.0-20230221065045-e58665ef23c7 // indirect
	gitlab.com/go-extension/hpke v0.0.0-20250212195157-716075a00b8a // indirect
	gitlab.com/go-extension/mlkem768 v0.0.0-20240814071630-937354a2177e // indirect
	gitlab.com/go-extension/rand v0.0.0-20240303103951-707937a049b5 // indirect
	golang.org/x/crypto v0.33.0 // indirect
	golang.org/x/sys v0.30.0 // indirect
)
