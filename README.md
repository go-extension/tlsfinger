# TLS Finger

This repository is an fingeprint extension of the [gitlab.com/go-extension/tls](https://pkg.go.dev/gitlab.com/go-extension/tls)

# Currently implements fingerprint

- Google Chrome 102 (PC with AES-NI)
- Microsoft Edge 106 (PC with AES-NI)
- Apple Safari 13

# Special thanks

- [uTLS](https://github.com/refraction-networking/utls)
