package tlsfinger

import (
	"fmt"
	"slices"
	"sort"
	"strings"

	"gitlab.com/go-extension/tls"
)

func JA3(ch *tls.ClientHelloInfo) string {
	versions := slices.DeleteFunc(slices.Clone(ch.SupportedVersions), tls.IsGREASE)
	sort.Slice(versions, func(i, j int) bool {
		return versions[i] > versions[j]
	})
	version := versions[0]

	suites := make([]uint16, 0, len(ch.CipherSuites))
	for _, suite := range ch.CipherSuites {
		if tls.IsGREASE(suite) {
			continue
		}
		suites = append(suites, suite)
	}

	extensions := make([]uint16, 0, len(ch.ExtensionList))
	for _, extension := range ch.ExtensionList {
		switch extension.(type) {
		case *tls.GREASEExtension:
		case *tls.ServerNameIndicationExtension:
		default:
			extensions = append(extensions, extension.ExtensionId())
		}
	}

	curves := make([]uint16, 0, len(ch.SupportedCurves))
	for _, curve := range ch.SupportedCurves {
		if tls.IsGREASE(uint16(curve)) {
			continue
		}
		curves = append(curves, uint16(curve))
	}

	return fmt.Sprintf("%d,%s,%s,%s,%s", version, strings.Join(toStringSlice(suites), "-"), strings.Join(toStringSlice(extensions), "-"), strings.Join(toStringSlice(curves), "-"), strings.Join(toStringSlice(ch.SupportedPoints), "-"))
}

func toStringSlice[T ~uint8 | ~uint16](items []T) []string {
	lists := make([]string, 0, len(items))
	for _, item := range items {
		lists = append(lists, fmt.Sprint(item))
	}
	return lists
}
