package tlsfinger

import "gitlab.com/go-extension/tls"

var emptyCache = &emptySessionCache{}

type emptySessionCache struct{}

func (*emptySessionCache) Get(sessionKey string) (session *tls.ClientSessionState, ok bool) {
	return nil, false
}

func (*emptySessionCache) Put(sessionKey string, cs *tls.ClientSessionState) {}
