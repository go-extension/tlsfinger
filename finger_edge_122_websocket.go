package tlsfinger

import (
	"gitlab.com/go-extension/tls"
)

var Finger_Edge_122_Websocket = Finger{
	JA3: "772,4865-4866-4867-49195-49199-49196-49200-52393-52392-49171-49172-156-157-47-53,11-35-65281-23-18-65037-5-10-13-16-17513-43-27-51-45-21,29-23-24,0",
	Config: &tls.Config{
		CipherSuites: []uint16{
			tls.GREASE_PLACEHOLDER,
			tls.TLS_AES_128_GCM_SHA256,
			tls.TLS_AES_256_GCM_SHA384,
			tls.TLS_CHACHA20_POLY1305_SHA256,
			tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256,
			tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256,
			tls.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA,
			tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
			tls.TLS_RSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_RSA_WITH_AES_128_CBC_SHA,
			tls.TLS_RSA_WITH_AES_256_CBC_SHA,
		},
		PreferCipherSuites: true,
		Renegotiation:      tls.RenegotiateOnceAsClient,
		CurvePreferences: []tls.CurveID{
			tls.GREASE_PLACEHOLDER,
			tls.X25519,
			tls.CurveP256,
			tls.CurveP384,
		},
		MinVersion: tls.VersionTLS12,
		MaxVersion: tls.VersionTLS13,
		SupportedVersions: []uint16{
			tls.GREASE_PLACEHOLDER,
			tls.VersionTLS13,
			tls.VersionTLS12,
		},
		CertCompressionPreferences: []tls.CertCompressionAlgorithm{
			tls.Brotli,
		},
		SignatureAlgorithmsPreferences: []tls.SignatureScheme{
			tls.ECDSAWithP256AndSHA256,
			tls.PSSWithSHA256,
			tls.PKCS1WithSHA256,
			tls.ECDSAWithP384AndSHA384,
			tls.PSSWithSHA384,
			tls.PKCS1WithSHA384,
			tls.PSSWithSHA512,
			tls.PKCS1WithSHA512,
		},
		NextProtos:         []string{"http/1.1"},
		NextProtoSettings:  map[string][]byte{"h2": nil},
		MinClientHelloSize: 512,
		ShuffleExtensions:  true,
		EncryptedClientHelloDummyConfig: &tls.ECHDummyConfig{
			CipherSuites: []tls.ECHCipher{
				{KEM: tls.KEM_X25519_HKDF_SHA256, KDF: tls.KDF_HKDF_SHA256, AEAD: tls.AEAD_AES128GCM},
				{KEM: tls.KEM_X25519_HKDF_SHA256, KDF: tls.KDF_HKDF_SHA256, AEAD: tls.AEAD_ChaCha20Poly1305},
			},
			HelloInnerLens: []uint16{128, 160, 192, 224},
		},
		Extensions: []tls.Extension{
			&tls.GREASEExtension{},
			&tls.ServerNameIndicationExtension{},
			&tls.SupportedPointsExtension{},
			&tls.SessionTicketExtension{},
			&tls.RenegotiationInfoExtension{},
			&tls.ExtendedMasterSecretExtension{},
			&tls.SCTExtension{},
			&tls.EarlyDataExtension{},
			&tls.QUICTransportParametersExtension{},
			&tls.EncryptedClientHelloExtension{},
			&tls.StatusRequestExtension{},
			&tls.SupportedGroupsExtension{},
			&tls.SignatureAlgorithmsExtension{},
			&tls.SignatureAlgorithmsCertExtension{},
			&tls.ALPNExtension{},
			&tls.ALPSExtension{},
			&tls.SupportedVersionsExtension{},
			&tls.CompressedCertificateExtension{},
			&tls.CookieExtension{},
			&tls.KeyShareExtension{},
			&tls.PSKModesExtension{},
			&tls.ECHOuterExtension{},
			&tls.GREASEExtension{},
			&tls.PaddingExtension{},
			&tls.PreSharedKeyExtension{},
		},
		ClientSessionCache: emptyCache,
	},
}
