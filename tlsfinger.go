package tlsfinger

import (
	"crypto/x509"
	"io"
	"time"

	"gitlab.com/go-extension/tls"
)

type Finger struct {
	JA3 string
	*tls.Config
}

var (
	Finger_Chrome         = Finger_Chrome_106
	Finger_Edge           = Finger_Edge_122
	Finger_Edge_Websocket = Finger_Edge_122_Websocket
	Finger_Safari         = Finger_Safari_16
)

type Config struct {
	Rand                                io.Reader
	Time                                func() time.Time
	Certificates                        []tls.Certificate
	NameToCertificate                   map[string]*tls.Certificate
	GetCertificate                      func(*tls.ClientHelloInfo) (*tls.Certificate, error)
	GetClientCertificate                func(*tls.CertificateRequestInfo) (*tls.Certificate, error)
	GetConfigForClient                  func(*tls.ClientHelloInfo) (*tls.Config, error)
	VerifyPeerCertificate               func(rawCerts [][]byte, verifiedChains [][]*x509.Certificate) error
	VerifyConnection                    func(tls.ConnectionState) error
	RootCAs                             *x509.CertPool
	ServerName                          string
	ClientAuth                          tls.ClientAuthType
	ClientCAs                           *x509.CertPool
	InsecureSkipVerify                  bool
	ClientSessionCache                  tls.ClientSessionCache
	UnwrapSession                       func(identity []byte, cs tls.ConnectionState) (*tls.SessionState, error)
	WrapSession                         func(tls.ConnectionState, *tls.SessionState) ([]byte, error)
	KeyLogWriter                        io.Writer
	KernelTX, KernelRX                  bool
	EncryptedClientHelloConfigList      []byte
	EncryptedClientHelloKeys            []tls.EncryptedClientHelloKey
	EncryptedClientHelloRejectionVerify func(tls.ConnectionState) error
}

func TLSConfig(finger Finger, config *Config) *tls.Config {
	if config == nil {
		return finger.Config
	}

	if finger.Config == nil {
		return &tls.Config{
			Rand:                                config.Rand,
			Time:                                config.Time,
			Certificates:                        config.Certificates,
			NameToCertificate:                   config.NameToCertificate,
			GetCertificate:                      config.GetCertificate,
			GetClientCertificate:                config.GetClientCertificate,
			GetConfigForClient:                  config.GetConfigForClient,
			VerifyPeerCertificate:               config.VerifyPeerCertificate,
			VerifyConnection:                    config.VerifyConnection,
			RootCAs:                             config.RootCAs,
			ServerName:                          config.ServerName,
			ClientAuth:                          config.ClientAuth,
			ClientCAs:                           config.ClientCAs,
			InsecureSkipVerify:                  config.InsecureSkipVerify,
			ClientSessionCache:                  config.ClientSessionCache,
			UnwrapSession:                       config.UnwrapSession,
			WrapSession:                         config.WrapSession,
			KeyLogWriter:                        config.KeyLogWriter,
			KernelTX:                            config.KernelTX,
			KernelRX:                            config.KernelRX,
			EncryptedClientHelloConfigList:      config.EncryptedClientHelloConfigList,
			EncryptedClientHelloRejectionVerify: config.EncryptedClientHelloRejectionVerify,
			EncryptedClientHelloKeys:            config.EncryptedClientHelloKeys,
		}
	}

	tlsConf := finger.Clone()
	tlsConf.Rand = config.Rand
	tlsConf.Time = config.Time
	tlsConf.Certificates = config.Certificates
	tlsConf.NameToCertificate = config.NameToCertificate
	tlsConf.GetCertificate = config.GetCertificate
	tlsConf.GetClientCertificate = config.GetClientCertificate
	tlsConf.GetConfigForClient = config.GetConfigForClient
	tlsConf.VerifyPeerCertificate = config.VerifyPeerCertificate
	tlsConf.VerifyConnection = config.VerifyConnection
	tlsConf.RootCAs = config.RootCAs
	tlsConf.ServerName = config.ServerName
	tlsConf.ClientAuth = config.ClientAuth
	tlsConf.ClientCAs = config.ClientCAs
	tlsConf.InsecureSkipVerify = config.InsecureSkipVerify
	tlsConf.UnwrapSession = config.UnwrapSession
	tlsConf.WrapSession = config.WrapSession
	tlsConf.KeyLogWriter = config.KeyLogWriter
	tlsConf.KernelTX = config.KernelTX
	tlsConf.KernelRX = config.KernelRX
	if tlsConf.EncryptedClientHelloDummyConfig != nil { // make sure finger supports encryptedClientHello
		tlsConf.EncryptedClientHelloConfigList = config.EncryptedClientHelloConfigList
		tlsConf.EncryptedClientHelloRejectionVerify = config.EncryptedClientHelloRejectionVerify
		tlsConf.EncryptedClientHelloKeys = config.EncryptedClientHelloKeys
	}

	if config.ClientSessionCache != nil {
		tlsConf.ClientSessionCache = config.ClientSessionCache
	}
	return tlsConf
}
